<?php

namespace App\Theme\Api;

interface HistoryManagementInterface
{
    public static function getBackUrl();
}