<?php

namespace App\Theme\Provider;

use App\Theme\Api\HistoryManagementInterface;
use App\Theme\Management\HistoryManagement;
use Illuminate\Support\ServiceProvider;

class HistoryContractServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            HistoryManagementInterface::class,
            HistoryManagement::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
