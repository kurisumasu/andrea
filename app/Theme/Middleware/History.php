<?php

namespace App\Theme\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class History
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    const SESSION_KEY = 'history';

    const EXCLUDED_ROUTES = [
        'datatable',
        'ajax',
        'back',
        'login',
        'register',
    ];

    const EXCLUDED_METHODS = [
        'post',
        'put',
        'update'
    ];

    public function handle(Request $request, Closure $next)
    {
        try {
            $currentUrl = strtolower($request->url());
            $currentMethod =  strtolower($request->method());

            foreach (self::EXCLUDED_ROUTES as $EXCLUDED_ROUTE) {
                if (strpos($currentUrl, $EXCLUDED_ROUTE) > 0) {
                    return $next($request);
                }
            }

            foreach (self::EXCLUDED_METHODS as $EXCLUDED_METHOD) {
                if (strpos($currentMethod, $EXCLUDED_METHOD) === 0) {
                    return $next($request);
                }
            }

            if (Session::has(self::SESSION_KEY)) {
                $array = Session::get(self::SESSION_KEY);

                $last = end($array);
                $prev = prev($array);

                if (($last == $prev) && ($last != null && $prev != null)) {
                    return $next($request);
                }
            }

            $array[] = $currentUrl;

            Session::put(self::SESSION_KEY, $array);
        } catch (\Exception $exception) {
            //Silence is golden
        }

        return $next($request);
    }
}
