<?php

if (!function_exists('get_route_name')) {
    function get_route_name()
    {
        $routeName = Route::currentRouteName();
        $class = str_replace( '.', '-', $routeName );
        return $class;
    }
}

if(!function_exists('client_has_permission_to_view')) {
    function client_has_permission_to_view()
    {
        if (!\Illuminate\Support\Facades\Auth::user()) {
           return false;
        }

        if(\Illuminate\Support\Facades\Auth::user()->customer->provider != \App\User\Model\User::WEBSITE_PROVIDER) {
            return false;
        }

        if (\Illuminate\Support\Facades\Auth::user()->can('Admin')) {
           return true;
        }

        if(request()->customerId == \Illuminate\Support\Facades\Auth::id()) {
            return true;
        }

        return false;
    }
}

if (!function_exists('get_errors_alert_html')) {
    function get_errors_alert_html($errors)
    {
        if (count($errors) <= 0) {
            return null;
        }

        return view('theme::template.limitless.components.errors', ['errors' => $errors->all()]);
    }
}

if (!function_exists('get_back_url_button')) {
    function get_back_url_button ($url)
    {
        return view('theme::template.dna.components.page.header.back', ['url' => $url])->render();
    }
}

if (!function_exists('get_terms_and_condition_modal')) {
    function get_terms_and_condition_modal ()
    {
        return view('theme::template.dna.components.info.modal.termsandcondition')->render();
    }
}

if (!function_exists('get_cookies_policy_modal')) {
    function get_cookies_policy_modal ()
    {
        return view('theme::template.dna.components.info.modal.cookiespolicy')->render();
    }
}

if (!function_exists('order_by')) {
    function order_by($data, $field)
    {
        $code = "return strnatcmp(\$a['$field'], \$b['$field']);";
        usort($data, create_function('$a,$b', $code));
        return $data;
    }
}

if (!function_exists('get_base_url')) {
    function get_base_url ($url)
    {
        $baseUrl = \Illuminate\Support\Str::lower($url);

        $baseUrl = str_replace("https://", "", $baseUrl);
        $baseUrl = str_replace("http://", "", $baseUrl);
        $baseUrl = str_replace("://", "", $baseUrl);
        $baseUrl = str_replace("//", "", $baseUrl);

        $exploded = explode('/', $baseUrl);

        if (count($exploded) > 1) {
            $baseUrl = $exploded[0];
        } else {
            $baseUrl = str_replace("/", "", $baseUrl);
        }

        return $baseUrl;
    }
}