<?php

namespace App\Theme\Management;

use App\Theme\Api\HistoryManagementInterface;
use App\Theme\Middleware\History;
use Illuminate\Support\Facades\Session;

class HistoryManagement implements HistoryManagementInterface
{
    public static function getBackUrl()
    {
        try {
            if (!Session::has(History::SESSION_KEY)) {
                return null;
            }

            $olderUrl = Session::get(History::SESSION_KEY);
            end($olderUrl);
            $url = prev($olderUrl);

            array_pop($olderUrl);
            array_pop($olderUrl);
            Session::put(History::SESSION_KEY, $olderUrl);

            return $url;
        } catch (\Exception $exception) {
            return null;
        }
    }
}