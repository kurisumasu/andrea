<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('theme::template.limitless.components.head')
</head>
<body id="{{get_route_name()}}">
    <script>
        console.log(localStorage.getItem('sidebarClass'));
        jQuery('body').addClass(localStorage.getItem('sidebarClass'));
    </script>
    @include('theme::template.limitless.components.navbar')

    <div class="page-content">

        @include('theme::template.limitless.components.sidebar')

        <div class="content-wrapper">

            <div class="page-header page-header-light @section('page.header.class') @show">
                <div class="page-header-content header-elements-inline">
                    @section('page.header.back')
                    @show
                    <div class="page-title">
                        <h5 class="page-title-text">
                            @yield('page.header.title')
                        </h5>
                    </div>

                    <div class="header-elements">
                        @section('page.header.elements')
                            @show
                    </div>
                </div>
            </div>

            @section('page.custom.navbar')
            @show

            <div class="content @yield('content.style')">

                @section('content')
                    @show

            </div>

            @include('theme::template.limitless.components.footer')

        </div>

    </div>

</body>
</html>

@section('scripts')
    @show