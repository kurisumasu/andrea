<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name') }}</title>

<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="{{ asset('css/fonts/barlow/font.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/fonts/furore/font.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/icons/material/styles.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/layout.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/components.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/colors.min.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('css/extras/radio_color_picker.css') }}" rel="stylesheet" type="text/css">

<script src="{{ asset('js/main/jquery.min.js') }}"></script>
<script src="{{ asset('js/main/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ asset('js/plugins/ui/moment/moment.min.js') }}"></script>


<script src="{{asset('js/app.js')}}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ asset('js/plugins/forms/selects/select2totree.js') }}"></script>
<script src="{{ asset('js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
<script src="{{ asset('js/plugins/pickers/daterangepicker.js') }}"></script>
<script src="{{ asset('js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
<script src="{{ asset('js/plugins/forms/styling/switch.min.js') }}"></script>
<script src="{{ asset('js/plugins/extensions/jquery_ui/core.min.js')}}"></script>
<script src="{{ asset('js/plugins/extensions/jquery_ui/effects.min.js')}}"></script>
<script src="{{ asset('js/plugins/extensions/jquery_ui/interactions.min.js')}}"></script>

<link href="{{ asset('css/dnafactory-custom.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/alert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/button.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/dropdown.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/input.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/table.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/select2totree.css') }}" rel="stylesheet" type="text/css">

<script src="{{ asset('js/plugins/tables/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('js/dnafactory/locales/it/datatables/config.js') }}"></script>
<script src="{{ asset('js/dnafactory/datatables/pipeline.js') }}"></script>

<script src="{{ asset('js/plugins/trees/fancytree_all.min.js')}}"></script>
<script src="{{ asset('js/plugins/trees/fancytree_childcounter.js')}}"></script>
<script src="{{ asset('js/dnafactory/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{ asset('js/dnafactory/jquery-validation/additional-methods.js')}}"></script>
<script src="{{ asset('js/dnafactory/jquery-validation/localization/messages_it.js')}}"></script>
<script src="{{ asset('js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
<script src="{{ asset('js/plugins/uploaders/fileinput/plugins/purify.min.js')}}"></script>
<script src="{{ asset('js/plugins/uploaders/fileinput/plugins/sortable.min.js')}}"></script>

<script src="{{ asset('js/plugins/velocity/velocity.min.js')}}"></script>
<script src="{{ asset('js/plugins/velocity/velocity.ui.min.js')}}"></script>
<script src="{{ asset('js/plugins/ui/prism.min.js')}}"></script>

<script src="{{ asset('js/plugins/pickers/color/spectrum.js')}}"></script>
<script src="{{ asset('js/plugins/pickers/anytime.min.js')}}"></script>

<script src="{{asset('js/dnafactory/echarts/echarts.js')}}"></script>
<script src="{{asset('js/dnafactory/echarts/custom/echarts_lines_basic.js')}}"></script>
<script src="{{asset('js/dnafactory/echarts/custom/echarts_pie_basic.js')}}"></script>
<script src="{{asset('js/dnafactory/save_plugin/config.js')}}"></script>

<script>
    if (localStorage.getItem('expanderIconClass') === '') {
        localStorage.setItem('expanderIconClass', 'fa-minus');
    }
</script>