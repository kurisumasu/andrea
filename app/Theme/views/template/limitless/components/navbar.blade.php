<div class="navbar navbar-expand-md navbar-light">
    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <div class="sidebar-extender btn-primary">
            <i class="fas fa-minus"></i>
        </div>
        <script>
            jQuery('.sidebar-extender i').addClass(localStorage.getItem('expanderIconClass'));
        </script>
        <div class="navbar-brand navbar-brand-md">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="{{ asset('media/logos/logo_dna.png') }}" alt="">
            </a>
        </div>

        <div class="navbar-brand navbar-brand-xs">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="{{ asset('media/logos/pictogram_dna.png') }}" alt="">
            </a>
        </div>
    </div>

    <div class="d-flex flex-1 d-md-none">
        <div class="navbar-brand mr-auto">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="{{ asset('media/logos/logo_dna_dark.png') }}" style="height: 30px!important;" alt="">
            </a>
        </div>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-tree5"></i>
        </button>

        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-paragraph-justify3"></i>
        </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
        <div class="mr-md-auto"></div>

        <ul class="navbar-nav">
            @guest
                <div class="nav-item">

                    @if(Request::route()->getName() == "login")

                        <a href="{{route('register')}}" class="btn btn-icon btn-tertiary">
                            <i class="fas fa-user-plus"></i>
                            <span>{{__('Crea account')}}</span>
                        </a>

                    @else

                        <a href="{{route('login')}}" class="btn btn-icon btn-tertiary">
                            <i class="fas fa-user-plus"></i>
                            <span>{{__('Accedi')}}</span>

                        </a>
                    @endif

                </div>
            @endguest

            @auth

                <a href="{{route('logout')}}" class="btn btn-icon btn-tertiary">
                    <i class="fas fa-sign-out-alt"></i>
                    <span>{{__('Logout')}}</span>
                </a>

            @endauth
        </ul>
    </div>
</div>

<script>
    let body = jQuery('body');
    let expander = jQuery('.sidebar-extender');

    expander.on('click', function () {
        body.toggleClass('sidebar-xs');
        changeExpanderIcon();
    });
    
    function changeExpanderIcon() {
        if (body.hasClass('sidebar-xs')) {
            expander.find('i').removeClass('fa-minus');
            expander.find('i').addClass('fa-plus');
            localStorage.setItem("sidebarClass", 'sidebar-xs');
            localStorage.setItem("expanderIconClass", "fa-plus");
            return;
        }
        expander.find('i').removeClass('fa-plus');
        expander.find('i').addClass('fa-minus');
        localStorage.setItem("sidebarClass", '');
        localStorage.setItem("expanderIconClass", "fa-minus");
    }
</script>
