<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">
    <div class="sidebar-content">
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">
                <li class="nav-item-header"><div class="text-uppercase nav-item-header-text">Menu</div></li>
                <li class="nav-item"><a href="" id="nav-home" class="nav-link"><i class="fas fa-home"></i><span><?php echo __('Aggiungi Photo')?></span></a></li>
            </ul>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        let bodyId = document.body.id;
        let routes = bodyId.split("-");
        let parent = routes[0];
        $('#nav-' + parent).addClass('active');
        $('#nav-' + parent).closest('.nav-item-submenu').addClass('nav-item-open');
        $('#nav-' + parent).siblings('.nav-group-sub').css('display', 'block');
        $('.nav-sidebar').find('.nav-item').each(function () {
            let linkId = String(jQuery(this).find('a').prop('id'));
            if (linkId.indexOf(parent) >= 0) {
                jQuery(this).find('a').addClass('active');
            }
        });
    });
</script>