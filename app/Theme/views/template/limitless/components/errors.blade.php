<div class="alert alert-danger alert-styled-left alert-dismissible">
    <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
    <ul class="m-0">
        @foreach($errors as $error)
            <li>
                {{$error}}
            </li>
        @endforeach
    </ul>
</div>