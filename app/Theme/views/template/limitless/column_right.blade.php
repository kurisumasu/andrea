<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('theme::template.limitless.components.head')
</head>
<body id="{{get_route_name()}}">
<script>
    console.log(localStorage.getItem('sidebarClass'));
    jQuery('body').addClass(localStorage.getItem('sidebarClass'));
</script>
@include('theme::template.limitless.components.navbar')

<div class="page-content">

    @include('theme::template.limitless.components.sidebar')

    <div class="content-wrapper">

        <div class="page-header page-header-light @section('page.header.class') @show">
            <div class="page-header-content header-elements-inline">
                @section('page.header.back')
                @show
                <div class="page-title">
                    <h5 class="page-title-text">
                        @yield('page.header.title')
                    </h5>
                </div>

                <div class="header-elements">
                    @section('page.header.elements')
                    @show
                </div>
            </div>
        </div>

        @section('page.custom.navbar')
        @show

        <div class="content @yield('content.style')">

            @section('content')
            @show

        </div>

        @include('theme::template.limitless.components.footer')

    </div>

    <div class="sidebar sidebar-light sidebar-right sidebar-expand-md">
        <div class="sidebar-mobile-toggler text-center">
            <a href="#" class="sidebar-mobile-expand">
                <i class="icon-screen-full"></i>
                <i class="icon-screen-normal"></i>
            </a>
            <span class="font-weight-semibold">Right sidebar</span>
            <a href="#" class="sidebar-mobile-right-toggle">
                <i class="icon-arrow-right8"></i>
            </a>
        </div>
        <div class="sidebar-content">
        </div>
    </div>

</div>

</body>
</html>

@section('scripts')
@show