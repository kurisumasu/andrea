<div id="cookiespolicy_modal" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Policy sui Cookies')}}</h5>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vestibulum elit eget felis lobortis volutpat. Nunc volutpat eu arcu vulputate scelerisque. Proin varius eros tincidunt urna tempor dapibus. Nam porta libero placerat tincidunt efficitur. Vestibulum non leo mauris. Quisque dictum lacinia tincidunt. Donec aliquam mauris risus, et interdum ligula congue quis. In hac habitasse platea dictumst.

                    Pellentesque id consequat turpis. Pellentesque ac urna non augue convallis egestas. In pharetra dui non lacinia scelerisque. Nunc eget finibus dui. Suspendisse eu turpis aliquet, varius augue nec, sodales est. Sed nec felis sed eros auctor semper. Integer vehicula ante sit amet lorem iaculis facilisis.

                    Sed nec augue tellus. Quisque tincidunt vitae purus eget viverra. Suspendisse hendrerit eleifend ante eu iaculis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus in varius ante. Duis sed justo sed mauris laoreet eleifend. Aliquam orci eros, euismod scelerisque tempus nec, lobortis ac urna. Mauris cursus cursus libero quis sollicitudin. Cras dapibus porta ligula vel finibus.

                    In id iaculis massa. Suspendisse potenti. Nulla a nibh mollis nisi elementum tincidunt eget id velit. Fusce efficitur et ante ut tempus. Suspendisse quis sem semper, tristique elit eget, sodales elit. Suspendisse hendrerit magna id felis sagittis, fringilla semper sem condimentum. Praesent dignissim placerat efficitur.

                    Vestibulum placerat sapien vitae tempus luctus. Nunc molestie nulla id tempus fermentum. Maecenas venenatis justo purus, non pellentesque augue tempus id. Morbi eget lorem eu elit finibus cursus vel nec tortor. Vestibulum finibus nulla urna, a ornare lectus fermentum rhoncus. Pellentesque at leo id ex tincidunt congue vitae sit amet magna. Ut et cursus metus.</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">{{__('Chiudi')}}</button>
            </div>
        </div>
    </div>
</div>