<?php

namespace App\Core\Seed;

use App\Acl\Model\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Core\Model\Config;

class ConfigsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
        ];


        foreach ($items as $item) {
            DB::table(Config::TABLE)->insert($item);
        }
    }
}
