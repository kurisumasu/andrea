<?php

namespace App\Core\Provider;

use App\Core\Api\ConfigFactoryInterface;
use App\Core\Api\Data\ConfigInterface;
use App\Core\Api\ConfigRepositoryInterface;
use App\Core\Api\Data\LanguageInterface;
use App\Core\Api\LanguageFactoryInterface;
use App\Core\Api\LanguageRepositoryInterface;
use App\Core\Factory\ConfigFactory;
use App\Core\Factory\LanguageFactory;
use App\Core\Model\Config;
use App\Core\Model\Language;
use App\Core\Repository\ConfigRepository;
use App\Core\Repository\LanguageRepository;
use Illuminate\Support\ServiceProvider;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            ConfigInterface::class,
            Config::class
        );

        $this->app->singleton(
            ConfigRepositoryInterface::class,
            ConfigRepository::class
        );

        $this->app->singleton(
            ConfigFactoryInterface::class,
            ConfigFactory::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
