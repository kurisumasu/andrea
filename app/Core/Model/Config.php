<?php

namespace App\Core\Model;

use App\Core\Api\Data\ConfigInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Config
 * @package App\Core\Model
 * @property string code
 * @property string value
 * @property boolean system
 */
class Config extends Model implements ConfigInterface
{
    const TABLE = 'configs';
    public $table = self::TABLE;

    const FOREIGN = 'config_id';

    public $timestamps = false;

    protected $fillable = ['code', 'value', 'system'];

    protected $casts = [
        'code' => 'string',
        'value' => 'string',
        'system' => 'boolean',
    ];
}
