<?php

namespace App\Core\Api;

interface ConfigFactoryInterface
{
    public function create(string $code, $value, $update = false, array $params = []);
    public function make(string $code, $value, array $params = []);
}
