<?php

Route::group(['middleware' => ['web']], function () {
    // Authentication Routes
    Route::get('login', 'App\Auth\Controller\LoginController@showLoginForm')->name('login');
    Route::post('login', 'App\Auth\Controller\LoginController@login');
    Route::get('logout', 'App\Auth\Controller\LoginController@logout')->name('logout');

    // Registration Routes
    Route::get('register', 'App\Auth\Controller\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'App\Auth\Controller\RegisterController@register');

    // Password Reset Routes
    Route::get('password/reset', 'App\Auth\Controller\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'App\Auth\Controller\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'App\Auth\Controller\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'App\Auth\Controller\ResetPasswordController@reset');
});

