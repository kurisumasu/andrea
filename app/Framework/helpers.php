<?php

function array_keys_exist(array $needles, array $haystack)
{
    foreach ($needles as $needle)
    {
        if ( ! array_key_exists($needle, $haystack)) return false;
    }

    return true;
}