<style>
    body {
        font-size: 20px!important;
    }
</style>
@foreach($classes as $class)
    <h1>{{$class['alias']}}</h1>
    <h3>{{$class['class']->getName()}}</h3>
    <p>
        @foreach($class['class']->getMethods() as $method)
        {{$method->getName()}} (
            @foreach($method->getParameters() as $param)
                <i>{{$param->getType()}}</i> <strong>{{$param->getName()}}</strong> @if(!$loop->last)
                    ,
                @endif
            @endforeach

            )

            <br />
        @endforeach
    </p>
    <p></p>

@endforeach