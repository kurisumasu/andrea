<?php

namespace App\Framework\Provider;

use App\Framework\Model\Log;
use Illuminate\Support\ServiceProvider;
use Psr\Log\LoggerInterface;
use App\Framework\Api\SearchCriteria;
use App\Framework\Api\SearchCriteriaInterface;
use App\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use App\Framework\Api\SearchCriteria\CollectionProcessor;

class ContractsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            LoggerInterface::class,
            Log::class
        );

        $this->app->bind(
            SearchCriteriaInterface::class,
            SearchCriteria::class
        );

        $this->app->bind(
            SearchCriteria\CollectionProcessorInterface::class,
            SearchCriteria\CollectionProcessor::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
