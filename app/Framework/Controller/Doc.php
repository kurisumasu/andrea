<?php

namespace App\Framework\Controller;

use App\Http\Controllers\Controller;

class Doc extends Controller
{
    public function index()
    {
        $webApiFile = app_path("webapi.php");
        $webApiArray = [];

        if (file_exists($webApiFile)) {
            $webApiArray = include app_path("webapi.php");
        }

        $classes = [];

        foreach ($webApiArray as $key => $webApi) {
            $classes[] = ['class' => new \ReflectionClass($webApi), 'alias' => $key];
        }

        return view("framework::doc.index", ['classes' => $classes]);
    }
}