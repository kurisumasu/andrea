<?php

namespace App\Framework\Controller\Rest;

use App\Framework\Api\SearchCriteriaBuilder;
use App\Framework\Api\SearchCriteriaInterface;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $allowedRepositories = [

    ];

    protected function getClassAlias($className)
    {
        if (!array_key_exists($className, $this->allowedRepositories)) {
            throw new \Exception(__("API non abilitata"));
        }

        return $this->allowedRepositories[$className];
    }

    public function manager(Request $request, $className, $methodName)
    {
        $webApiFile = app_path("webapi.php");
        if (file_exists($webApiFile)) {
            $webApiArray = include app_path("webapi.php");
            $this->allowedRepositories = array_merge($this->allowedRepositories, $webApiArray);
        }

        $params = [];

        try {
            $classAlias = $this->getClassAlias($className);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'data' => '', 'message' => $e->getMessage()]);
        }

        $classConcrete = app()->make($classAlias);

        $reflection = new \ReflectionMethod($classAlias, $methodName);
        $classParams = $reflection->getParameters();
        foreach ($classParams as $classParam) {
            $paramName = $classParam->getName();

            $paramValue = $request->input($paramName, null);
            if (!$classParam->isOptional() && $paramValue == null) {
                $message =__("Parametro :paramName mancante", ['paramName' => $paramName]);
                return response()->json(['status' => 0, 'data' => '', 'message' => $message]);
            }

            $class = $classParam->getClass();
            if ($class) {
                $paramValue = $this->getValueFromClass($class->getName(), $paramValue);
            } else {
                $type = $classParam->getType();
                settype($paramValue, $type);
            }

            $params[] = $paramValue;
        }

        try {
            $return = $classConcrete->{$methodName}(...$params);
        } catch (\Exception $e) {
            return response()->json(['status' => 0, 'data' => '', 'message' => $e->getMessage()]);
        }

        //$return = call_user_func_array([$classConcrete, $methodName], $params);
        return response()->json(['status' => 1, 'data' => $return, 'message' => '']);
    }

    protected function getValueFromClass($className, $value)
    {
        $class = app()->make($className);
        switch(true) {
            case $class instanceof Model:
                $model = $class->find($value['id']);
                $columns = \Schema::getColumnListing($class->getTable());
                foreach ($columns as $column) {
                    if (array_key_exists($column, $value)) {
                        $model->{$column} = $value[$column];
                    } else {
                        $model->unset($column);
                    }
                }

                return $model;
                break;
            case $class instanceof SearchCriteriaInterface:
                $searchCriteriaBuilder = app()->make(SearchCriteriaBuilder::class);
                $searchCriteriaBuilder->buildFromArray($value);
                return $searchCriteriaBuilder->create();
                break;
        }

        return $class;
    }
}
