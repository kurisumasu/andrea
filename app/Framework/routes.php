<?php

Route::get('/heartbeat', function (){
   return view('framework::heartbeat');
});

Route::prefix('/api/rest/{className}/{methodName}')->group(function () {
    Route::any('/', '\App\Framework\Controller\Rest\ApiController@manager');
});

Route::prefix('/doc')->group(function () {
    Route::get('/', '\App\Framework\Controller\Doc@index');
    Route::get('/detail/{class}', '\App\Framework\Controller\Doc@detail');
});