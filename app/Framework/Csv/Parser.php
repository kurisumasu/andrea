<?php

namespace App\Framework\Csv;

class Parser
{
    public static function buildFromRawData($csvData, $separator = ",", $enclosure = '"')
    {
        $csv = [];

        $csvData = trim($csvData);
        $rows = explode("\n", $csvData);
        $header = $rows[0];
        $header = str_getcsv($header, $separator);

        foreach ($header as &$item) {
            $item = trim($item);
            $item = str_replace($enclosure, "", $item);
            $item = str_replace(' ', "_", $item);
        }

        array_shift($rows);

        foreach ($rows as $row) {
            try {
                $tmp = str_getcsv($row, $separator, $enclosure);
                $tmp = array_combine($header, $tmp);
                $csv[] = $tmp;
            } catch (\Exception $e) {

            }
        }

        return $csv;
    }
}
