<?php

namespace App\Framework\Csv;

class Reader
{
    public function readFromFile(string $pathToFile, int $length = 0, string $delimiter = ',')
    {
        $row = 0;
        $data = [];
        $headers = [];

        try {
            if (($handle = fopen($pathToFile, "r")) !== FALSE) {
                while (($datum = fgetcsv($handle, $length, $delimiter)) !== FALSE) {
                    if ($row == 0) {
                        foreach ($datum as $header) {
                            $headers[] = $header;
                        }
                        $row++;
                        continue;
                    }

                    foreach ($datum as $key => $value) {
                        try {
                            $data[$row-1][$headers[$key]] = $value;
                        } catch (\Exception $exception) {

                        }
                    }

                    $row++;
                }
                fclose($handle);
            }

            return $data;
        } catch (\Exception $exception) {
            return array();
        }
    }
}