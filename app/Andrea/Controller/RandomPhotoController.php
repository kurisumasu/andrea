<?php

namespace App\Andrea\Controller;

use App\Andrea\Api\PhotoFactoryInterface;
use App\Andrea\Api\PhotoManagementInterface;
use App\Andrea\Api\PhotoRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RandomPhotoController extends Controller
{

    protected $photoManagement;
    protected $photoFactory;

    public function __construct
    (
        PhotoManagementInterface $photoManagement,
        PhotoFactoryInterface $photoFactory
    )
    {
        $this->photoFactory = $photoFactory;
        $this->photoManagement = $photoManagement;
    }

    public function save(Request $request)
    {
        $name = $request->input("name");

        $photo = $this->photoManagement->add($name);

        var_dump($photo);
        die();

        return back()
            ->with('success','You have successfully upload image.');
    }

    public function create(Request $request)
    {
        return View("andrea::create");
    }

    public function index(Request $request)
    {
        try {
            $photo = $this->photoManagement->getRandom();
        } catch (\Exception $e) {
            $photo = null;
        }

        return View("andrea::random.show", ["photo" => $photo]);
    }
}
