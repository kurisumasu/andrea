<?php

namespace App\Andrea\Controller\Api;

use App\Andrea\Api\PhotoManagementInterface;
use App\Andrea\Api\PhotoRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RandomPhotoController extends Controller
{

    protected $photoManagement;

    public function __construct
    (
        PhotoManagementInterface $photoManagement
    )
    {
        $this->photoManagement = $photoManagement;
    }

    public function index(Request $request)
    {

        try {
            $photo = $this->photoManagement->getRandom();

            $result = [
                "status" => "ok",
                "data" => [
                    "photo" => $photo
                ]
            ];
        } catch (\Exception $e) {
            $result = [
                "status"  => "error",
                "message" => $e->getMessage()
            ];
        }


        return json_encode($result);
    }
}
