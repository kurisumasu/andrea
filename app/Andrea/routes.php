<?php

Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => '/api', 'as' => 'api.'], function () {
        Route::group(['prefix' => '/andrea', 'as' => 'andrea.'], function () {
            Route::get('/random', ["as" => "random", "uses" => "\App\Andrea\Controller\Api\RandomPhotoController@index"]);
        });
    });

    Route::group(['prefix' => '/andrea', 'as' => 'andrea.'], function () {
        Route::get('/random', ["as" => "random", "uses" => "\App\Andrea\Controller\RandomPhotoController@index"]);
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::group(['prefix' => '/andrea', 'as' => 'andrea.'], function () {
            Route::get('/', ["as" => "create", "uses" => "\App\Andrea\Controller\RandomPhotoController@create"]);
            Route::post('/', ["as" => "save", "uses" => "\App\Andrea\Controller\RandomPhotoController@save"]);
        });
    });
});