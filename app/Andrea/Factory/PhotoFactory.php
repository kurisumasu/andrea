<?php

namespace App\Andrea\Factory;

use App\Andrea\Api\PhotoFactoryInterface;
use App\Andrea\Api\PhotoRepositoryInterface;
use App\Andrea\Model\Photo;

class PhotoFactory implements PhotoFactoryInterface
{

    protected $photoRepository;

    public function __construct
    (
        PhotoRepositoryInterface $photoRepository
    )
    {
        $this->photoRepository = $photoRepository;
    }

    public function make($name)
    {
        return factory(Photo::class)->make([
            'name'  => $name,
        ]);
    }

    public function create($name)
    {
        $photo = $this->make($name);
        return $this->photoRepository->save($photo);
    }
}

