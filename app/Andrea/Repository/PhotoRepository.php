<?php

namespace App\Andrea\Repository;

use App\Andrea\Api\Data\PhotoInterface;
use App\Andrea\Api\PhotoRepositoryInterface;

use App\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use App\Framework\Api\SearchCriteriaInterface;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;


class PhotoRepository implements PhotoRepositoryInterface
{
    protected $_model;
    protected $_collectionProcessor;

    protected static $contractors = array();

    public function __construct(
        PhotoInterface $model,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->_model = $model;
        $this->_collectionProcessor = $collectionProcessor;
    }

    public function save(PhotoInterface $model)
    {
        $model->save();
        return $model;
    }

    public function get($code)
    {
        return $this->_model->where('code', $code)->firstOrFail();
    }

    public function getById($id)
    {
        return $this->_model->findOrFail($id);
    }

    public function delete(PhotoInterface $model)
    {
        return $model->delete();
    }

    public function deleteById($id)
    {
        $model = $this->getById($id);
        return $this->delete($model);
    }

    public function deleteByCode($code)
    {
        $model = $this->get($code);
        return $this->delete($model);
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $list = $this->_model->newQuery();
        $this->_collectionProcessor->process($searchCriteria, $list);

        return $list->get();
    }

    public function getByUserId($userId)
    {
        if (array_key_exists($userId, self::$contractors) && self::$contractors[$userId]) {
            return self::$contractors[$userId];
        }

        $user = User::findOrFail($userId);
        $contractor =  $user->contractors;

        self::$contractors[$userId] = $contractor;
        return self::$contractors[$userId];
    }
}
