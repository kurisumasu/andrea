<?php

namespace App\Andrea\Api;

interface PhotoFactoryInterface
{
    public function make($name);

    public function create($name);
}
