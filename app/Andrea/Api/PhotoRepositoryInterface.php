<?php

namespace App\Andrea\Api;

use App\Andrea\Api\Data\PhotoInterface;
use App\Framework\Api\SearchCriteriaInterface;

interface PhotoRepositoryInterface
{
    public function save(PhotoInterface $model);

    public function get($code);

    public function getById($id);

    public function getByUserId($userId);

    public function delete(PhotoInterface $model);

    public function deleteById($id);

    public function deleteByCode($code);

    public function getList(SearchCriteriaInterface $searchCriteria);
}
