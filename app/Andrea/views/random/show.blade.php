@extends('theme::template.limitless.sidebar_default')

@section("page.header.title")
    {{__("Andrea a cazzo di Cane")}}
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-body">

                @if($photo)

                    <img src="{{$photo->url}}" alt="{{$photo->name}}"/>

                    <label class="name">{{$photo->name}}</label>

                @endif

            </div>
        </div>
    </div>

@endsection