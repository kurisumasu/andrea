@extends('theme::template.limitless.sidebar_default')

@section("page.header.title")
    {{__("Aggiungi un Andrea a cazzo di Cane")}}
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card card-body">

                <form method="POST" action="{{route("andrea.save")}}" _lpchecked="1">
                    <div class="modal-body">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome della foto') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-md-4 text-md-right">{{ __("Carica l'immagine") }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" name="image" class="form-control @error('image') is-invalid @enderror" required>


                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-tertiary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection