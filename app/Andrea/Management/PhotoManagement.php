<?php

namespace App\Andrea\Management;

use App\Andrea\Api\PhotoFactoryInterface;
use App\Andrea\Api\PhotoManagementInterface;
use App\Andrea\Api\PhotoRepositoryInterface;
use App\Framework\Api\SearchCriteriaBuilder;

class PhotoManagement implements PhotoManagementInterface
{

    protected $photoRepository;
    protected $photoFactory;

    public function __construct
    (
        PhotoRepositoryInterface $photoRepository,
        PhotoFactoryInterface $photoFactory
    )
    {
        $this->photoFactory = $photoFactory;
        $this->photoRepository = $photoRepository;
    }

    public function add($name)
    {
        $photo = $this->photoFactory->make($name);

        $photo->addMediaFromRequest('image')
            ->toMediaCollection('images');

        $this->photoRepository->save($photo);
        return $photo;
    }

    public function getRandom()
    {
        try {
            $photos = $this->photoRepository->getList(SearchCriteriaBuilder::all());
            return $photos->random();
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
