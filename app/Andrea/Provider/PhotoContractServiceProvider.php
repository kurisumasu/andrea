<?php

namespace App\Andrea\Provider;

use App\Andrea\Api\Data\PhotoInterface;
use App\Andrea\Api\PhotoFactoryInterface;
use App\Andrea\Api\PhotoManagementInterface;
use App\Andrea\Api\PhotoRepositoryInterface;
use App\Andrea\Factory\PhotoFactory;
use App\Andrea\Management\PhotoManagement;
use App\Andrea\Model\Photo;
use App\Andrea\Repository\PhotoRepository;
use Illuminate\Support\ServiceProvider;

class PhotoContractServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PhotoInterface::class,
            Photo::class
        );

        $this->app->singleton(
            PhotoRepositoryInterface::class,
            PhotoRepository::class
        );

        $this->app->singleton(
            PhotoFactoryInterface::class,
            PhotoFactory::class
        );

        $this->app->singleton(
            PhotoManagementInterface::class,
            PhotoManagement::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}