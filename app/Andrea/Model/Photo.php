<?php

namespace App\Andrea\Model;

use App\Andrea\Api\Data\PhotoInterface;
use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Photo extends Model implements PhotoInterface,HasMedia
{
    use HasMediaTrait;

    const TABLE = 'andrea_photo';
    public $table = self::TABLE;

    const FOREIGN = 'andrea_photo_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
