<?php

namespace App\Http\Controllers\Resource;

use App\Core\resources\ResourceCollection;
class GenericCollection extends ResourceCollection
{
    protected $status;
    protected $_totalRecords;
    protected $_totalRecordsFiltered;
    public function __construct($resource, $totalRecords, $totalRecordsFiltered, int $status = 1, string $message = "Success")
    {
        $this->_totalRecords = $totalRecords;
        $this->_totalRecordsFiltered = $totalRecordsFiltered;
        parent::__construct($resource, $status, $message);
    }
    public function toArray($request)
    {
        return [
            "recordsFiltered" => $this->_totalRecordsFiltered,
            "recordsTotal" => $this->_totalRecords,
            "data" => $this->collection,
        ];
    }
}