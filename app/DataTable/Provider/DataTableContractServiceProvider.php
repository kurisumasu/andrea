<?php

namespace App\DataTable\Provider;

use App\DataTable\Api\DataTableRenderManagementInterface;
use App\DataTable\Api\DataTableSearchManagementInterface;
use App\DataTable\Management\DataTableRenderManagement;
use App\DataTable\Management\DataTableSearchManagement;
use Illuminate\Support\ServiceProvider;

class DataTableContractServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            DataTableSearchManagementInterface::class,
            DataTableSearchManagement::class
        );

        $this->app->singleton(
            DataTableRenderManagementInterface::class,
            DataTableRenderManagement::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
