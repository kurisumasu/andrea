<?php

namespace App\DataTable\Api;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface DataTableSearchManagementInterface
{
    public function getEntitiesByColumnsSearchValue(Model $model, array $dataTableRequest);

    public function getEntitiesByCollection(Collection $collection, array $dataTableRequest);

    public function addCustomFilter(array $dataArray, $key, $value);
}
