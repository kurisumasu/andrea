<?php

namespace App\DataTable\Api;

interface DataTableRenderManagementInterface
{
    public function getRender($modelClass, array $phpValues = null);
}
