<?php
        /** @var string $class*/
        $configuration = config('datatable.configuration.' . $class);

        $filters = null;

        
        if ($configuration['custom_filters']) {
            /** @var \App\Merchant\Api\MerchantProductLayeredNavigationManagementInterface $filtersManagement */
            $filtersManagement = app()->make(\App\Merchant\Api\MerchantProductLayeredNavigationManagementInterface::class);

            $filters = $filtersManagement->getFilters(request());
        }

        if ($customValues) {
            foreach ($customValues as $key => $customValue) {
                $filters[] = [
                    'data' => $key,
                    'value' => $customValue,
                    'custom' => true
                ];

            }
        }

        $tableName = $configuration['table_name'];
        $dataProvider = $configuration['ajax'];
        $dataColumns = $configuration['columns'];
        $dataColumnsDef = $configuration['columnDefs'];
        $dataSearchable = $configuration['searchableRenders'];

?>

<div id="{{{$tableName}}}-table-spinner" class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
<table class="table datatable-responsive datatable-basic table-bordered table-striped table-hover dataTable no-footer" id="{{$tableName}}-table" role="grid">
    <thead>
    <tr role="row">
        <?php
            foreach($dataColumns as $columnKey => $column) {
                $class = null;

                foreach ($dataColumnsDef as $columnDef) {
                    if(in_array($columnKey, $columnDef['targets']) && $columnDef['orderable']) {
                        $class .= ' sorting';
                    } else if (in_array($columnKey, $columnDef['targets']) && !$columnDef['orderable']) {
                        $class .= ' sorting-disabled';
                    }
                }
        ?>
            <th class="{{$class}}" data-name="{{$column}}" tabindex="0" aria-controls="{{$tableName}}-table" rowspan="1" colspan="1" style="width: 71.25px;">{{__($column)}}</th>
        <?php
            }
        ?>
    </tr>
    </thead>
</table>

<script>
    jQuery(document).ready(function () {
        let tableId = '#' + '{{$tableName}}' + '-table';
        let table = null;
        let messageWrapper = jQuery(tableId + '-message');
        let tableElement = jQuery(tableId);
        let spinner = jQuery(tableId + '-spinner');
        let columns = ["{!!implode('","', $dataColumns)!!}"];

       table = tableElement.on('xhr.dt', function ( e, settings, json, xhr ) {
           spinner.hide();
       } ).DataTable({
           "processing": true,
           "serverSide": true,
           "ajax": $.fn.dataTable.pipeline( {
                <?php
                        $dataUrl = [];
                        foreach ($dataProvider['data_to_send'] as $dataToSend) {
                            if (strpos($dataToSend['value'], 'request:') === 0) {
                                $value = str_replace('request:', '',$dataToSend['value']);
                                $dataUrl[$dataToSend['name']] = request()->{$value};
                            } else if (strpos($dataToSend['value'], 'php:') === 0) {
                                $value = str_replace('php:', '',$dataToSend['value']);
                                $dataUrl[$dataToSend['name']] = $customValues[$value];
                            } else {
                                $dataUrl[$dataToSend['name']] = $dataToSend['value'];
                            }
                        }
                ?>
               url: '{{ route($dataProvider['url'], $dataUrl)}}',
               pages: 10,
               @if($filters)
               data: function (data) {
                   data.filters = [];
                   @foreach($filters as $key => $filter)
                       @if(!$filter['custom'])
                           data.filters.push({
                               'data' : '{{$filter['data']}}',
                               'value' : parseFilterLayer(jQuery('#{{$tableName}}_{{$key}}_search'))
                           });
                        @continue
                       @endif
                        data.filters.push({
                            'data' : '{{$filter['data']}}',
                            'value' : '{{$filter['value']}}'
                        });
                   @endforeach
               },
               @endif
           }),
           "columns" : [
               @foreach($dataColumns as $column)
                    {"data" : "{{$column}}"},
               @endforeach
           ],
           "columnDefs" : [
               @foreach($dataColumnsDef as $columnDef)
                   {
                        "targets": [{{implode(',',$columnDef['targets'])}}],
                        "orderable": '{{$columnDef['orderable']}}',
                        "searchable": '{{$columnDef['searchable']}}',
                   },
               @endforeach
           ],
       });

        tableElement.find('th').each(function (i) {
            @foreach($dataSearchable as $datum)
                if([{{implode(',', $datum['targets'])}}].includes(i)) {
                    @if($datum['type'] == 'input')
                        jQuery(this).append('' +
                        '<input id="input-workaround" class="ml-2 form-control {{implode(' ', $datum['css_classes'])}}" type="text" placeholder="{{$datum['placeholder']}}">');
                    @elseif($datum['type'] == 'select')
                        jQuery(this).append('' +
                        '<select class="ml-2 form-control form-control-select2 select-input" id="' + columns[i] + '_search_dt" data-placeholder="{{$datum['placeholder']}}" data-fouc>"' +
                        '"<option value="-1">{{__('Tutti')}}</option>' +
                        @if(strpos($datum['options'], 'ajax:') !== 0)
                            @foreach($datum['options'] as $option)
                                '<option value="{{$option['value']}}">{{__($option['name'])}}</option>' +
                            @endforeach
                        @else
                            '<option disabled id="loading-options-' + columns[i] + '" value="-1">{{$datum['placeholder']}}</option>' +
                        @endif
                        '');

                        jQuery('#' + columns[i] + '_search_dt').select2({
                            theme: "{{implode(' ', $datum['css_classes'])}}",
                        });

                        @if(strpos($datum['options'], 'ajax:') === 0)
                            jQuery.ajax({
                                url: '{{route(str_replace('ajax:', '', $datum['options']))}}',
                            })
                                .done(function (data) {
                                    jQuery('#' + columns[i] + '_search_dt').select2('close');
                                    jQuery('#' + columns[i] + '_search_dt').append(data);
                                    jQuery('#loading-options-' + columns[i]).remove();
                                });
                        @endif
                    @elseif($datum['type'] == 'select_tree')
                    jQuery(this).append('' +
                        '<select class="ml-2 form-control form-control-select2 select_tree-input {{implode(' ', $datum['css_classes'])}}" id="' + columns[i] + '_search_dt" data-placeholder="{{$datum['placeholder']}}" data-fouc>"' +
                        '"<option value="-1">{{__('Tutti')}}</option>' +
                        @if(strpos($datum['options'], 'ajax:') !== 0)
                            @foreach($datum['options'] as $option)
                                '<option value="{{$option['value']}}">{{__($option['name'])}}</option>' +
                            @endforeach
                        @else
                            '<option disabled id="loading-options-' + columns[i] + '" value="-1">{{$datum['placeholder']}}</option>' +
                        @endif
                            '');

                        jQuery('#' + columns[i] + '_search_dt').select2ToTree({
                            theme: "{{implode(' ', $datum['css_classes'])}}",
                        });

                        @if(strpos($datum['options'], 'ajax:') === 0)
                            jQuery.ajax({
                                url: '{{route(str_replace('ajax:', '', $datum['options']))}}',
                            })
                                .done(function (data) {
                                    jQuery('#' + columns[i] + '_search_dt').select2('close');
                                    jQuery('#' + columns[i] + '_search_dt').append(data);
                                    jQuery('#loading-options-' + columns[i]).remove();
                                });
                        @endif
                    @endif
                }
            @endforeach
        });

        table.columns().every( function () {
            var that = this;

            $( 'input', this.header() ).on( 'keyup change clear', function () {
                spinner.show();
                if ( that.search() !== this.value ) {
                    that
                        .search( this.value )
                        .draw();
                }
            } );

            $( 'select', this.header() ).change(function () {
                spinner.show();
                if ( that.search() !== $(this).val() ) {
                    that
                        .search( $(this).val() )
                        .draw();
                }
            });
        });
    });

    function parseFilterLayer(filter)
    {
        let dataQueryString = '';
        try {
            dataQueryString = (filter.serializeFormJSON()[filter.attr('name')]).toString()
        } catch (error) {
        }

        return dataQueryString;
    }

    (function ($) {
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
    })(jQuery);
</script>
