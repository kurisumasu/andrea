<?php

return [

    /*
        Here you should define the logic between fields, this is used for DataTable Search

        HOW TO DEFINE A NEW DATATABLE SEARCH CONFIG:

        'table_name' => [
            'db_field_name' => 'logic_type'
            ...
        ]

        db_field_name, logic_type:
            -and
            -or
     */

    /*
    \App\Merchant\Model\Merchant::TABLE => [
        'name' => [
            'type' => 'and',
            'method' => 'null',
        ],
        'website' => [
            'type' => 'and',
            'method' => 'null',
        ],
        'industry_id' => [
            'type' => 'and',
            'method' => 'null',
        ],
        'cms_id' => [
            'type' => 'and',
            'method' => 'null',
        ],
    ],

    \App\Merchant\Model\Merchant\Product::TABLE => [
        'brand_id' => [
            'type' => 'relationships',
            'method' => 'brand',
            ],
        'name' => [
            'type' => 'and',
            'method' => 'null',
        ],
        'color' => [
            'type' => 'and',
            'method' => 'null',
        ],
        'merchant_sku' => [
            'type' => 'and',
            'method' => 'null',
        ],
        \App\Merchant\Model\Merchant::FOREIGN => [
            'type' => 'key',
            'method' => 'null',
        ],
    ],
        Here you should define the configuration for render a new DataTable

        HOW TO USE IT:

        'model_class' => [
            'table_name' => 'db_table_name',
            'custom_filters' => true or false,
            'ajax' => [
                'url' => 'data_provider_url',
                'data_to_send' => [
                    [
                        'name' => 'field_name',
                        'value' => request or custom
                    ],
                    ...
                ]
            ],
            'columns' => [
                'db_field_name',
                ...
            ],
            'columnDefs' => [
                [
                    'targets' => array,
                    'orderable' => true or false (string),
                    'searchable' => true or false (string),
                ],
                ...
            ],
            'searchableRenders' => [
                [
                    'targets' => array,
                    'type' => 'input_type',
                    'placeholder' => 'input_placeholder',
                    'css_classes' => [
                        'css_class',
                        ...
                    ]
                    'options' => ajax or custom
                ],
                ...
            ]
        ]

        ajax, data_to_send, value:
                -request:param_name
                -custom => [
                    'param_name' => 'value'
                ]

        columnDefs, targets:
                -array = [0 , 1, ...] columns data key to trigger

        searchableRenders, targets:
                -array = [0 , 1, ...] columns data key to trigger

        searchableRenders, type:
                -input
                -select
                -select_tree

        searchableRenders, options:
                -ajax:ajax_url
                -custom = [
                    [
                        'name' => 'displayed_name'
                        'value' => 'option_value',
                    ],
                    ...
                [

     */

    "wallet" => [
        'table_name' => 'wallet',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.wallet.index',
            'data_to_send' => [
                [
                    'name' => 'walletId',
                    'value' => 'request:walletId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'test',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],


    "transactions" => [
        'table_name' => 'transactions',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.transaction.index',
            'data_to_send' => [
                [
                    'name' => 'transactionId',
                    'value' => 'request:transactionId'
                ]
            ]
        ],
        'columns' => [
            'amount',
            'type',
            'wallet',
            'typology',
            'payment-actor',
            'tax'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3, 4, 5],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],


    "wallet_transactions" => [
        'table_name' => 'transactions',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.transaction.wallet',
            'data_to_send' => [
                [
                    'name' => 'transactionId',
                    'value' => 'request:transactionId'
                ]
            ]
        ],
        'columns' => [
            'amount',
            'type',
            'typology',
            'payment-actor',
            'tax'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3, 4],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],


    "recurring_transactions" => [
        'table_name' => 'recurring_transactions',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.recurring-transaction.index',
            'data_to_send' => [
                [
                    'name' => 'transactionId',
                    'value' => 'request:transactionId'
                ]
            ]
        ],
        'columns' => [
            'amount',
            'type',
            'typology',
            'payment-actor',
            'startDate',
            'endDate',
            'frequency',
            'tax'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "typologies" => [
        'table_name' => 'typologies',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.typology.index',
            'data_to_send' => [
                [
                    'name' => 'transactionId',
                    'value' => 'request:transactionId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'color',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "customers" => [
        'table_name' => 'customers',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.customer.index',
            'data_to_send' => [
                [
                    'name' => 'customerId',
                    'value' => 'request:customerId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'email',
            'phone',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "employees" => [
        'table_name' => 'employees',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.employee.index',
            'data_to_send' => [
                [
                    'name' => 'employeesId',
                    'value' => 'request:employeesId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'email',
            'phone',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "providers" => [
        'table_name' => 'providers',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.provider.index',
            'data_to_send' => [
                [
                    'name' => 'providersId',
                    'value' => 'request:providersId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'email',
            'phone',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "tax" => [
        'table_name' => 'tax',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.tax.index',
            'data_to_send' => [
                [
                    'name' => 'taxId',
                    'value' => 'request:taxId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'percentage',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    "wallets_tax" => [
        'table_name' => 'wallets_tax',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.tax.wallets',
            'data_to_send' => [
                [
                    'name' => 'taxId',
                    'value' => 'request:taxId'
                ]
            ]
        ],
        'columns' => [
            'wallet_name',
            'taxable_balance',
            'not_taxable_balance',
            'tax_balance'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => [
        ]
    ],

    /*\App\Merchant\Model\Merchant::class => [
        'table_name' => 'merchant',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.merchant.index',
            'data_to_send' => [
                [
                    'name' => 'merchantId',
                    'value' => 'request:merchantId'
                ]
            ]
        ],
        'columns' => [
            'name',
            'website',
            'industry_id',
            'cms_id',
            'actions'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3],
                'orderable' => 'true',
                'searchable' => 'true',
            ],
            [
                'targets' => [4],
                'orderable' => 'false',
                'searchable' => 'false',
            ]
        ],
        'searchableRenders' => [
            [
                'targets' => [0, 1],
                'type' => 'input',
                'placeholder' => __('Digita per cercare...'),
                'css_classes' => [
                    'dataTable_searchbox'
                ]
            ],
            [
                'targets' => [2],
                'type' => 'select_tree',
                'placeholder' => __('Caricamento in corso...'),
                'options' => 'ajax:ajax.industries.options.index',
                'css_classes' => [
                    'search_datatable'
                ]
            ],
            [
                'targets' => [3],
                'type' => 'select',
                'placeholder' => __('Caricamento in corso...'),
                'options' => 'ajax:ajax.cms.options.index',
                'css_classes' => [
                    'search_datatable'
                ]
            ]
        ]
    ],

    \App\Merchant\Model\Merchant\Product::class => [
        'table_name' => 'products',
        'custom_filters' => true,
        'ajax' => [
            'url' => 'datatable.merchant.products',
            'data_to_send' => [
                [
                    'name' => 'merchantId',
                    'value' => 'request:merchantId'
                ],
            ]
        ],
        'columns' => [
            'name',
            'size',
            'color',
            'merchant_sku',
            'price',
            'action'
        ],
        'columnDefs' => [
            [
                'targets' => [1, 4],
                'orderable' => 'true',
                'searchable' => 'false',
            ],
            [
                'targets' => [5],
                'orderable' => 'false',
                'searchable' => 'false',
            ],
            [
                'targets' => [0, 2, 3],
                'orderable' => 'true',
                'searchable' => 'true',
            ]
        ],
        'searchableRenders' => [
            [
                'targets' => [0, 3],
                'type' => 'input',
                'placeholder' => __('Digita per cercare...'),
                'css_classes' => [
                    'dataTable_searchbox',
                ],
            ],
        ]
    ],

    (\App\Merchant\Model\Merchant\Product::class . \App\Merchant\Model\Merchant\Brand::class) => [
        'table_name' => 'products-brand',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.brand.products',
            'data_to_send' => [
                [
                    'name' => 'brandId',
                    'value' => 'php:brandId'
                ],
            ]
        ],
        'columns' => [
            'name',
            'merchant_sku',
            'price',
            'action'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3],
                'orderable' => 'true',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => []
    ],
    (\App\Merchant\Model\Merchant\Product::class . \App\Merchant\Model\Merchant\Category::class) => [
        'table_name' => 'products-category',
        'custom_filters' => false,
        'ajax' => [
            'url' => 'datatable.category.products',
            'data_to_send' => [
                [
                    'name' => 'categoryId',
                    'value' => 'php:categoryId'
                ],
            ]
        ],
        'columns' => [
            'name',
            'merchant_sku',
            'price',
            'action'
        ],
        'columnDefs' => [
            [
                'targets' => [0, 1, 2, 3],
                'orderable' => 'true',
                'searchable' => 'false',
            ],
        ],
        'searchableRenders' => []
    ],
    */
];
