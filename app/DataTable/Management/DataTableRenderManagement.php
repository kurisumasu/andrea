<?php

namespace App\DataTable\Management;

use App\DataTable\Api\DataTableRenderManagementInterface;

class DataTableRenderManagement implements DataTableRenderManagementInterface
{
    public function getRender($modelClass, array $customValues = null)
    {
        return view('datatable::render', ['class' => $modelClass, 'customValues' => $customValues])->render();
    }
}
