<?php

namespace App\DataTable\Management;

use App\DataTable\Api\DataTableSearchManagementInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DataTableSearchManagement implements DataTableSearchManagementInterface
{
    public function getEntitiesByColumnsSearchValue(Model $model, array $dataTableRequest)
    {
        try {
            $configuration = config('datatable.configuration.' . $model->getTable());

            //Get query by DataTable Filters
            $entities = $this->getSearchedEntities($model, $dataTableRequest, $configuration);

            return $entities;
        } catch (\Exception $exception) {
            var_dump($exception);
            return array();
        }
    }

    public function addCustomFilter(array $dataArray, $key, $value)
    {
        array_push($dataArray['columns'], [
            'data' => $key,
            'name' => $key,
            'searchable' => 'true',
            'orderable' => 'false',
            'search' => [
                'value' => $value,
                'regex' => 'false'
            ],
        ]);

        return $dataArray;
    }

    public function getEntitiesByCollection(Collection $collection, array $dataTableRequest)
    {
        try {
            /** @var Model $model */
            $model = $collection->first();

            if ($model) {
                $configuration = config('datatable.configuration.' . $model->getTable());

                //Get query by DataTable Filters
                $entities = $this->getSearchedEntitiesCollection($collection, $dataTableRequest, $configuration);
                return $entities;
            }

            return collect();
        } catch (\Exception $exception) {
            return null;
        }
    }

    protected function getSearchedEntitiesCollection(Collection $collection, array $dataTableRequest, $configuration)
    {
        $entities = $collection;

        //TODO::dna:: capire come gestire i filtri su collection
        /*$entities = $collection->filter(function ($item) use ($dataTableRequest, $configuration) {
            foreach ($dataTableRequest['columns'] as $column) {
                //DataTable sends this info as a string
                if ($column['searchable'] == 'false') {
                    continue;
                }

                if ($column['search']['value'] < 0) {
                    $column['search']['value'] = '';
                }

                if ($configuration[$column['data']] == 'and') {
                    $item->whereIn($column['data'], $column['search']['value']);
                }

                if ($configuration[$column['data']] == 'or') {
                    $item->whereIn($column['data'], $column['search']['value']);
                }
            }
        });*/

        //Get query by Custom Filters, optional
        /*if (array_key_exists('filters', $dataTableRequest) && count($dataTableRequest['filters']) > 0) {
            $entities->filter(function ($query) use ($dataTableRequest, $configuration) {
                foreach ($dataTableRequest['filters'] as $customFilter) {
                    $filterExploded = explode(',', $customFilter['value']);

                    foreach ($filterExploded as $value) {
                        if ($value < 0) {
                            $value = '';
                        }

                        if ($configuration[$customFilter['data']] == 'and') {
                            $query->whereIn($customFilter['data'], 'LIKE', '%' . $value . '%');
                        }

                        if ($configuration[$customFilter['data']] == 'or') {
                            $query->whereIn($customFilter['data'], 'LIKE', '%' . $value . '%');
                        }
                    }
                }
            });
        }*/

        $orderRule = $dataTableRequest['order'][0];
        $triggeredColumn = $dataTableRequest['columns'][$orderRule['column']];

        $sorted = collect();

        if ($triggeredColumn['orderable'] == 'true') {
            switch ($orderRule['dir']) {
                case 'asc':
                    $sorted = $entities->sortBy($triggeredColumn['data']);
                    break;
                case 'desc':
                    $sorted = $entities->sortByDesc($triggeredColumn['data']);
                    break;
                default:
                    $sorted = $entities->sortBy($triggeredColumn['data']);
            }
        }

        return $sorted->slice($dataTableRequest['start'])->values()->take($dataTableRequest['length']);
    }

    protected function getSearchedEntities($model, array $dataTableRequest, $configuration)
    {
        $entities = $model->where(function ($query) use ($dataTableRequest, $configuration) {
            foreach ($dataTableRequest['columns'] as $column) {
                try {
                    //DataTable sends this info as a string
                    if ($column['searchable'] == 'false') {
                        continue;
                    }

                    if ($column['search']['value'] < 0) {
                        $column['search']['value'] = '';
                    }

                    if ($configuration[$column['data']]['type'] == 'and') {
                        $query->where($column['data'], 'LIKE', '%'.$column['search']['value'].'%');
                    }

                    if ($configuration[$column['data']]['type'] == 'or') {
                        $query->orWhere($column['data'], 'LIKE', '%'.$column['search']['value'].'%');
                    }

                    if ($configuration[$column['data']]['type'] == 'key') {
                        $query->where($column['data'], 'LIKE', $column['search']['value']);
                    }

                } catch (\Exception $exception) {
                    continue;
                }
            }
        });


        //Get query by Custom Filters, optional
        if (array_key_exists('filters', $dataTableRequest) && count($dataTableRequest['filters']) > 0) {
            $entities->where(function ($query) use ($dataTableRequest, $configuration) {
                foreach ($dataTableRequest['filters'] as $customFilter) {
                    $filterExploded = explode(',', $customFilter['value']);
                    foreach ($filterExploded as $value) {
                        if ($value < 0 || $value == '') {
                            $value = '%%';
                        }

                        if ($configuration[$customFilter['data']]['type'] == 'and') {
                            $query->where($customFilter['data'], 'LIKE', '%' . $value . '%');
                        }

                        if ($configuration[$customFilter['data']]['type'] == 'or') {
                            $query->orWhere($customFilter['data'], 'LIKE', '%' . $value . '%');
                        }

                        if ($configuration[$customFilter['data']]['type'] == 'key') {
                            $query->where($customFilter['data'], 'LIKE', $value);
                        }

                        if ($configuration[$customFilter['data']]['type'] == 'relationship') {
                            $query->whereHas($configuration[$customFilter['data']]['method'], function (Builder $query) use ($value){
                                $query->where('id', 'LIKE', $value);
                            });
                        }

                        if ($configuration[$customFilter['data']]['type'] == 'relationships') {
                            $query->orWhereHas($configuration[$customFilter['data']]['method'], function (Builder $query) use ($value){
                                $query->where('id', 'LIKE', $value);
                            });
                        }
                    }
                }
            });
        }

        //Order by DataTable
        $orderRule = $dataTableRequest['order'][0];
        $triggeredColumn = $dataTableRequest['columns'][$orderRule['column']];

        if ($triggeredColumn['orderable'] == 'true') {
            $entities->orderBy($triggeredColumn['data'], $orderRule['dir']);
        }

        return $entities->take($dataTableRequest['length'])->skip($dataTableRequest['start'])->get();
    }
}
