<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;

class DiscoverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $items = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(__DIR__ . "/.."));
        foreach ($items as $item) {
            /** @var \SplFileInfo $item */

            if ($this->isMigrationFolder($item)) {
                $this->loadMigrationsFrom($item->getRealPath());
            }

            if ($this->isRoute($item)) {
                $this->loadRoutesFrom($item->getRealPath());
            }

            if (($viewNamespace = $this->isViewFolder($item)) !== false) {
                $this->loadViewsFrom($item->getRealPath(), $viewNamespace);
            }

            if ($this->isFactoryFolder($item)) {
                try {
                    $this->app->make(Factory::class)->load($item->getRealPath());
                } catch (\Exception $e) {

                }
            }

            if (($configNamespace = $this->isConfigFolder($item)) !== false) {
                $this->mergeConfigFrom($item, $configNamespace);
            }

            if ($this->isHelperFolder($item)) {
                require_once $item;
            }

            $this->registerServiceProvider($item);
        }
    }

    protected function isHelperFolder($modulePath)
    {
        if (strpos($modulePath, "helpers.php") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 3) {
            return false;
        }

        if ($path[0] != '.' || $path[2] != 'helpers.php') {
            return false;
        }

        return true;
    }

    protected function isMigrationFolder($modulePath)
    {
        if (strpos($modulePath, "migrations") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 4) {
            return false;
        }

        if ($path[0] != '.' || $path[3] != '.' || $path[2] != 'migrations') {
            return false;
        }

        return true;
    }

    protected function isConfigFolder($modulePath)
    {
        if (strpos($modulePath, "configs") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 4) {
            return false;
        }

        if ($path[0] != '.' || $path[3] == '.' || $path[3] == '' || $path[2] != 'configs') {
            return false;
        }

        return strtolower($path[1]) . "." . str_replace(".php", "", $path[3]);
    }

    protected function isRoute($modulePath)
    {
        if (strpos($modulePath, "routes.php") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 3) {
            return false;
        }

        if ($path[0] != '.' || $path[2] != 'routes.php') {
            return false;
        }

        return true;
    }

    protected function isViewFolder($modulePath)
    {
        if (strpos($modulePath, "views") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 4) {
            return false;
        }

        if ($path[0] != '.' || $path[3] != '.' || $path[2] != 'views') {
            return false;
        }

        return strtolower($path[1]);
    }

    protected function registerServiceProvider($modulePath)
    {
        if (strpos($modulePath, "ServiceProvider") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        if (strpos($path, "Providers") !== false) {
            return false;
        }

        $path = str_replace(".php", "", $path);
        $path = str_replace(".", "App", $path);
        $path = str_replace("/", "\\", $path);

        $this->app->register($path);

        return true;
    }

    protected function isFactoryFolder($modulePath)
    {
        if (strpos($modulePath, "factories") === false) {
            return false;
        }

        if (strpos($modulePath, "..") !== false) {
            $path = explode("..", $modulePath);
            $path = "." . $path[1];
        }

        $path = explode("/", $path);
        if (count($path) != 4) {
            return false;
        }

        if ($path[0] != '.' || $path[3] != '.' || $path[2] != 'factories') {
            return false;
        }

        return true;
    }
}
