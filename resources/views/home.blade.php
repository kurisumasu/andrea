@extends('theme::template.limitless.sidebar_default')

@section("page.header.title")
    {{__("I miei Conti")}}
@endsection

@section('page.header.elements')
        <a href="#" class="btn btn-primary"><i class="fas fa-wallet mr-2"></i> {{__("Aggiungi Conto")}}</a>
@endsection

@section('content')

        <div class="row">
            <div class="col-12 card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">

                            <div class="wallet-card col-lg-4">
                                <a href="/wallet/1">
                                    <div class="card btn-primary">
                                        <div class="card-body">

                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="d-flex">
                                                        <h3 class="font-weight-semibold mb-0">
                                                            $18,390
                                                        </h3>
                                                    </div>
                                                    <div>
                                                        Il mio 1° Conto
                                                        <div class="font-size-sm opacity-75">$37,578 avg</div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="card-icon-wrapper">
                                                        <i class="fas fa-wallet mr-2"></i>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </a>
                            </div>

                    </div>
                </div>
            </div>
        </div>

@endsection
