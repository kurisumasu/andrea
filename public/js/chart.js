let dataYAxis = [2.0 ,2.0, 5.0, 17.4, 20, 43];
let dataXAxis = ['10 Settembre','11 Settembre','12 Settembre','14 Settembre','15 Settembre','16 Settembre',
    '17 Settembre','18 Settembre','19 Settembre','20 Settembre','22 Settembre','23 Settembre', '24 Settembre',
    '25 Settembre', '26 Settembre', '27 Settembre'];

option = {
    tooltip : {
        trigger: 'axis',
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#DBDDDE',
        textStyle: {
            color: '#212121',
            fontFamily: 'Roboto',
            fontWeight: 'regular',
        },
        renderMode: 'html',
        formatter: function (params, ticket, callback) {
            let oldValue = '';
            let percentage = '';
            let percentageColor = '';
            let currentValue = params[0]['value'];
            let dataIndex = params[0]['dataIndex'];
            if (dataYAxis[dataIndex-1] !== currentValue && dataIndex !== 0) {
                oldValue = dataYAxis[dataIndex-1];
                if (currentValue > oldValue) {
                    percentage = (((currentValue - oldValue)/currentValue)*100).toFixed(2);
                } else {
                    percentage = (((oldValue - currentValue)/oldValue)*-100).toFixed(2);
                }
                if (percentage > 0) {
                    percentageColor = '#17A672'
                } else if (percentage < 0) {
                    percentageColor = '#F44336'
                } else {
                    percentageColor = '#DBDDDE'
                }
                oldValue = '€' + oldValue;
                percentage = percentage + '%';
            }


            return '<div style="color: #8C8C8C">' + params[0]['axisValueLabel'] + '</div>' +
                '<div style="font-weight: 500"><div style="text-decoration: line-through;">' + oldValue + '</div>' +
                '<div>€ '+ params[0]['value'] +'<span style="margin-left: 5px; color: ' + percentageColor + '">' +percentage  + '</span></div></div>';
        }
    },
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : dataXAxis,
        }
    ],
    yAxis : [
        {
            nameTextStyle: {
                color: 'red',
            },
            splitNumber: 10,
            type : 'value',
            splitLine: {
                lineStyle: {
                    color: '#F4F6F7',
                }
            },
            axisLine: {
                lineStyle: {
                    color: '#A1A1A1',
                },
            },
        }
    ],
    visualMap: {
        type: 'piecewise',
        show: false,
        dimension: 0,
        seriesIndex: 0,
        pieces: [ //to define
            {
                min: 0,
                max: 2,
                color: '#DBDDDE',
            },
            {
                min: 2,
                max: 9,
                color: '#17A672',
            },
            {
                min: 9,
                max: 13,
                color: '#F44336',
            },
            {
                min: 13,
                max: 15,
                color: '#DBDDDE',
            },

        ],

    },
    backgroundColor: '#FFFFFF',
    series : [
        {
            symbol: 'none',
            showSymbol: false,
            cursor: 'pointer',
            name:'Prezzo (€)',
            type:'line',
            connectNulls: true,
            step: 'middle',
            lineStyle: {
                width: 2,
            },
            areaStyle: {
                color: '#F4F6F7',
                opacity: 0.5,
            },
            data:dataYAxis,
        },

    ],

};
