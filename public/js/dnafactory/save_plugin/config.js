let btn_save = null;
let changes = null;

function initializeTriggerChanges(btnSelector) {
    btn_save = jQuery(btnSelector);

    jQuery('input').each(function(){
        if ($(this).hasClass('select2-search__field')) {
            return;
        }
        $(this).attr('prev', $(this).val());
    });

    jQuery('select').each(function(){
        $(this).attr('prev', $(this).val());
    });

    jQuery(document).ready(function () {

        jQuery('input').keyup(function () {
            if ($(this).hasClass('select2-search__field')) {
                return;
            }
            triggerChanges();
        });

        jQuery('select').change(function () {
            triggerChanges();
        });
    });
}

function triggerChanges() {

    jQuery('input').each(function () {
        if ($(this).hasClass('select2-search__field')) {
            return;
        }
        var prev = $(this).attr('prev');
        var current = $(this).val();

        if (prev !== current) {
            changes = true;
        }
    });

    jQuery('select').each(function () {
        var prev = $(this).attr('prev');
        var current = $(this).val();

        if (prev !== current) {
            changes = true;
        }
    });

    if (changes) {
        btn_save.prop('disabled', false);
        changes = false;
        return;
    }
    btn_save.prop('disabled', true);
}