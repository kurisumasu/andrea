$(document).ready(function () {
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        responsive: true,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtra: </span> _INPUT_',
            searchPlaceholder: 'Digita per cercare...',
            lengthMenu: '<span>Mostra:</span> _MENU_',
            paginate: { 'first': 'Primo', 'last': 'Ultimo', 'next': 'Avanti', 'previous': 'Indietro' },
            decimal:        "",
            emptyTable:     "Nessun dato disponibile",
            info:           "Mostrando _START_ su _END_ di _TOTAL_ risultati totali",
            infoEmpty:      "Nessun risultato",
            infoFiltered:   "(Filtrati _MAX_ risultati)",
            infoPostFix:    "",
            thousands:      ",",
            loadingRecords: "Caricamento...",
            processing:     "Processando...",
            zeroRecords:    "Nessun risultato trovato",
            aria: {
                "sortAscending":  ": attivato ordinamento crescente",
                "sortDescending": ": attivato ordinamento decrescente"
            }
        }
    });
});